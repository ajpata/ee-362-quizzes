#include <stdlib.h>
#include <stdio.h>

#define INFTY 1000

struct adjmatrix {
    int numnodes;
    int ** matrix;
};

void shortest_path(struct adjmatrix * am, int ** cost, int ** pred);
void display_path(int ** cost, int ** pred, int node1, int node2);
void previous(int ** cost, int ** pred, int node1, int node2);

struct adjmatrix * create_adjmatrix_1();
void destroy_adjmatrix(struct adjmatrix * am);
int ** matrix_create(int n);
void matrix_destroy(int ** matrix, int n);
void matrix_copy(int ** to, int ** from, int n);
void matrix_init(int ** matrix, int value, int n);
int min(int i, int j);

void main()
{
struct adjmatrix * am = create_adjmatrix_1();
int ** cost = matrix_create(am->numnodes); 
matrix_init(cost, INFTY,am->numnodes);
int ** pred = matrix_create(am->numnodes);
shortest_path(am, cost, pred);
display_path(cost, pred, 0,3);

matrix_destroy(cost,am->numnodes);
matrix_destroy(pred,am->numnodes);
destroy_adjmatrix(am);
}

void shortest_path(struct adjmatrix * am, int ** cost, int ** pred)
{
   int ** newcost = matrix_create(am->numnodes); 

   matrix_copy(cost, am->matrix, am->numnodes);

   for(int k = 2; k < am->numnodes; k++) {
      for(int u = 0; u < am->numnodes; u++) {
         for(int v = 0; v < am->numnodes;v++) {
            newcost[u][v] = cost[u][v];
            for(int w = 0; w < am->numnodes; w++) {
               if((w != u) && (newcost[u][v] > cost[u][w] + am->matrix[w][v])) {
                  newcost[u][v] = cost[u][w] + am->matrix[w][v];
                  pred[u][v] = w;
               }
            }
         }
      }
      matrix_copy(cost, newcost, am->numnodes);
   }

   matrix_destroy(newcost, am->numnodes);
}

void display_path(int ** cost, int ** pred, int node1, int node2)
{
   printf("Cost of path from node %d to node %d = %d\n",node1,node2,cost[node1][node2]);
   printf("Path: ");
   previous(cost, pred, node1, node2);
   printf("%d\n", node2);
}

void previous(int ** cost, int ** pred, int node1, int node2) {
   if(pred[node1][node2] == node1) {
      printf("%d ", node1);
      return;
   }
   previous(cost, pred, node1, pred[node1][node2]);
   printf("%d ", pred[node1][node2]);
}

struct adjmatrix * create_adjmatrix_1()
{
struct adjmatrix * am = (struct adjmatrix *)malloc(sizeof(struct adjmatrix));
am->numnodes=6;
am->matrix = matrix_create(am->numnodes);
matrix_init(am->matrix,INFTY,am->numnodes);

am->matrix[0][1]=1;
am->matrix[0][5]=6;

am->matrix[1][0]=1;
am->matrix[1][2]=3;
am->matrix[1][4]=1;

am->matrix[2][1]=3;
am->matrix[2][3]=4;
am->matrix[2][5]=1;

am->matrix[3][2]=1;

am->matrix[4][2]=1;
am->matrix[4][3]=4;
am->matrix[4][5]=3;

am->matrix[5][1]=1;
am->matrix[5][3]=1;
am->matrix[5][4]=1;

return am;
}

void destroy_adjmatrix(struct adjmatrix * am)
{
matrix_destroy(am->matrix, am->numnodes);
free(am);
}

int ** matrix_create(int n)
{
int ** matrix = (int **) malloc(n*sizeof(int *));
for (int i=0; i<n; i++) {
    matrix[i] = (int *) malloc(n*sizeof(int));
}
return matrix;
}

void matrix_destroy(int ** matrix, int n)
{
for (int i=0; i<n; i++) {
    free(matrix[i]);
}
free(matrix);
}

void matrix_copy(int ** to, int ** from, int n)
{
for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
        to[i][j] = from[i][j];
    }
}
}

void matrix_init(int ** matrix, int value, int n)
{
for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
        matrix[i][j] = value;
    }
}
}

int min(int i, int j)
{ 
if (i<j) return i;
return j;
}
