#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 37
#define E 100000000
#define M 53

unsigned int modexp(unsigned int n, unsigned int e, unsigned int m)
{
unsigned int c = 1;

while(e>0) {
   if(e&1) {
      c = (c*n)%m;
   }
   e = e>>1;
   n = (n*n)%m;
}

return c;
}

int main()
{
printf("%d^%d mod %d = %d\n", N, E, M, modexp(N,E,M));
}
