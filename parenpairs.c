#include <stdlib.h>
#include <stdio.h>

int parenpairs(int n)
{
   if(n==0) {
      return 1;
   } else {
      int c = 0;
      for(int k = 0; k < n; k++) {
         c+= parenpairs(k)*parenpairs(n-1-k);
      }
      return c;
   }
}

void displayParenPairs(int n)
{
printf("C(%d) = %d\n",n,parenpairs(n));
}


void main()
{
displayParenPairs(3);
displayParenPairs(5);
displayParenPairs(7);
displayParenPairs(11);
}
