#include <stdlib.h>
#include <stdio.h>

int binomial(int n, int k)
{
   if(k == 0 || k == n) {
      return 1;
   } else {
      return binomial(n-1, k-1) + binomial(n-1, k);
   }
}

void displayBinomial(int n, int k)
{
printf("Binomial coefficient (n=%d,k=%d) = %d\n",n,k,binomial(n,k));
}

int catalan(int n)
{
   if(n == 0) {
      return 1;
   } else {
      return (2*(2*n-1)*catalan(n-1))/(n+1);
   }
}

int catalan2(int n)
{
if (n<1) return 0;
return binomial(2*n,n)/(n+1);
}

void displayCatalan(int n)
{
printf("Catalan number (n=%d): catalan=%d, catalan2=%d\n",n,catalan(n),catalan2(n));
}

void main()
{
displayBinomial(10,2);
displayBinomial(10,3);
displayBinomial(10,5);
displayBinomial(10,7);
displayBinomial(10,8);

displayCatalan(3);
displayCatalan(5);
displayCatalan(10);
}
