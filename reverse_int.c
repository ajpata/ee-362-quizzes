#include <stdlib.h>
#include <stdio.h>

int reverse_int(int n)
{
int a = 0;

while(n!=0) {
   a *= 10;
   a += n%10;
   n /= 10;
}

return a;
}

int main()
{
int m = 456;
printf("n = %d, reverse = %d\n", m, reverse_int(m));
m = -456;
printf("n = %d, reverse = %d\n", m, reverse_int(m));
m = 3400;
printf("n = %d, reverse = %d\n", m, reverse_int(m));


}
