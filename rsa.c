#include <stdlib.h>
#include <stdio.h>

#define P 53
#define Q 47
#define MESSAGE 1234

/* modexp returns (n^e)%m */
unsigned int modexp(unsigned n, unsigned int e, unsigned int m)
{
unsigned int result = 1;
unsigned int t = n;
for (unsigned int k=e; k!=0; k=k>>1) {
    if (k&1 ==1) {
	    result = (result*t)%m;
    }
    t = (t*t)%m;
}
return result;
}

/* neg2posMod returns a positive version of n mod m */
unsigned int neg2posMod(int n, unsigned int m)
{
unsigned int result;
if (n>=0) {
    result = n%m;
}
else {
    unsigned int temp = -n;
    result = m-(temp%m);
}
return result;
}

/* Make this function work */
unsigned int extEuclidean(unsigned int phi, unsigned int e)
{
   unsigned int k = 0;
   unsigned int r0 = phi;
   unsigned int r1 = e;
   unsigned int s0 = 0;
   unsigned int s1 = 1;
   unsigned int t0 = 1;
   unsigned int t1 = 0;
   unsigned int q = 0;
   unsigned int temp;

   //printf("k= %d, r= %d, s=%d, t=%d, es+phit=%d\n",k,r0,s0,t0,e*s0 + phi*t0);

while(r1 != 0) {
   q = r0/r1;
   temp = r1;
   r1 = r0-q*r1;
   r0 = temp;
   temp = s1;
   s1 = s0-q*s1; 
   s0 = temp;
   temp = t1;
   t1 = t0-q*t1;
   t0 = temp;
   k += 1;

   //printf("k= %d, r= %d, s=%d, t=%d, es+phit=%d\n",k,r0,s0,t0,e*s0 + phi*t0);
}
   return s0;
}


int main()
{
unsigned int phi = (P-1)*(Q-1);
unsigned int e = 41*43;
unsigned int n = P*Q;

printf("Parameters:  P=%d, Q=%d, n=%d, phi=%d\n",P,Q,n,phi);

unsigned int s = extEuclidean(phi,e);
unsigned int d = neg2posMod(s,phi);

printf("Verify e and d are inverses:  e=%d, d=%d, phi=%d, (e*d) mod phi=%d\n",e,d,phi,(e*d)%phi);

unsigned int c = modexp(MESSAGE,e,n);
unsigned int y = modexp(c,d,n);
printf("Message=%d, Encrypted=%d, Decrypted=%d\n",MESSAGE,c,y);		
}

