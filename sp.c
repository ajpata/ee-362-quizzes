#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define NUMNODES 8

#define NoPred -1
#define INFINITY 1000

enum boolean {TRUE, FALSE};

struct graphNode { /* Used for graph nodes */
        enum boolean visited;
	int pred;   /* Predecesor */
	int dist;   /* Distance */
};

struct listNode {  /* Used in adjacency list */
	int id;
	int weight;
	struct listNode * next;
};

struct graph { 
	int numNodes;
	struct graphNode * node;
        struct listNode ** adjList;
};

struct graph * createGraph(int n) 
{
struct graph * g = (struct graph *) malloc(sizeof(struct graph));
g->numNodes = n;
g->adjList = (struct listNode **) malloc(sizeof(struct listNode *)*n);
for (int i=0; i<n; i++) {
    g->adjList[i] = NULL;
}
g->node = (struct graphNode *) malloc(n*sizeof(struct graphNode));
return g;
}

void destroyGraph(struct graph * g)
{
struct listNode * p;
struct listNode * temp;
for (int i=0; i<g->numNodes; i++) {
    for (p=g->adjList[i]; p!=NULL;) {
       temp=p;
       p=p->next;
       free(temp);
    }
}
if (g->node != NULL) {
    free(g->node);
}
free(g);
}

struct listNode * createListNode(int id)
{
struct listNode *p = (struct listNode *) malloc(sizeof(struct listNode));
p->id=id;
p->next=NULL;
return p;
}

struct listNode * insertNodeInList(struct listNode * list, struct listNode * new)
{
if (list==NULL) {
   return new;
}
struct listNode * p = list;
for (; p->next!=NULL; p=p->next) {
}
p->next = new;
new->next = NULL;
return list;
}

struct graph * insertUndirectedLink(struct graph * g, int i, int j, int weight)
{
struct listNode * p = createListNode(j);
p->weight = weight;
g->adjList[i] = insertNodeInList(g->adjList[i], p);
p = createListNode(i);
p->weight = weight;
g->adjList[j] = insertNodeInList(g->adjList[j], p);
return g;
}

struct graph * createGraph1()
{
struct graph * graph1= createGraph(NUMNODES);
graph1 = insertUndirectedLink(graph1,0,1,1);
graph1 = insertUndirectedLink(graph1,0,4,2);
graph1 = insertUndirectedLink(graph1,0,5,1);
graph1 = insertUndirectedLink(graph1,1,2,1);
graph1 = insertUndirectedLink(graph1,2,3,1);
graph1 = insertUndirectedLink(graph1,2,4,4);
graph1 = insertUndirectedLink(graph1,3,7,1);
graph1 = insertUndirectedLink(graph1,4,6,3);
graph1 = insertUndirectedLink(graph1,4,7,1);
graph1 = insertUndirectedLink(graph1,5,6,4);
graph1 = insertUndirectedLink(graph1,6,7,1);
return graph1;
}

void sp(struct graph * g, int source) {
   for(int i = 0; i < g->numNodes; i++) { /* Initialize node stats */ 
      g->node[i].pred= NoPred;
      g->node[i].visited= FALSE;
      g->node[i].dist= INFINITY;
   }
   g->node[source].dist = 0;
   g->node[source].visited = TRUE;

   for(int n = 1; n < g->numNodes; n++) {
      int u;
      int min = INFINITY;
      for(int i = 0; i < g->numNodes; i++) {
         if(g->node[i].visited == FALSE && g->node[i].dist < min) {
            min = g->node[i].dist;
            u = i;
         }
      }
      g->node[u].visited=TRUE;
     
      /* visit unvisited neighbors of current node */
      for(struct listNode * p = g->adjList[u]; p != NULL; p = p->next) {
         int v = p->id;
         if(g->node[v].visited == FALSE && g->node[v].dist > g->node[u].dist + p->weight) {
            g->node[v].pred = u;
            g->node[v].dist = g->node[u].dist + p->weight;
         }
      }
   }
}

void displayGraph(struct graph * g)
{

printf("Adjacencu List:  Node ID (Link Weight)\n");
struct listNode * p;
for (int i=0; i<g->numNodes; i++) {
   printf("%2d:",i);
   for (p=g->adjList[i]; p!= NULL; p=p->next) {
      printf("->%2d(%2d)",p->id,p->weight);
   }
   printf("\n");
}
printf("\n");

printf("Node stats\n");
for (int i=0; i<g->numNodes; i++) {
   printf("Node %2d: pred= %4d dist=%4d\n",i,g->node[i].pred,g->node[i].dist);
}
}

int main()
{
struct graph * g = createGraph1();
sp (g, 0);
displayGraph(g);

destroyGraph(g);
}
