#include <stdlib.h>
#include <stdio.h>

int climb(int n) {
   if(n==0) return 0;

   int ways[n+1];
   ways[0] = 1;
   ways[1] = 1;
   ways[2] = 2;
   for(int i = 3; i <= n; i++) {
      if(i-4 >= 0) {
         ways[i] = ways[i-1] + ways[i-2] + ways[i-4];
      } else {
         ways[i] = ways[i-1] + ways[i-2];
      }
   }
   return ways[n];
}

void main()
{
for (int n=0; n<20; n++) {
    printf("# ways for height %d = %d\n",n,climb(n));
}
}
