#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

struct node {
	int id;
	struct node * next;
};

struct node ** createAdjList(int n) /* Create adjacency list of of n nodes */
{
struct node ** adjList = (struct node **) malloc(sizeof(struct node *)*n);
for (int i=0; i<n; i++) {
    adjList[i] = NULL;
}
return adjList;
}

void destroyAdjList(struct node ** adjList, int n)
{
struct node * p;
struct node * temp;
for (int i=0; i<n; i++) {
    for (p=adjList[i]; p!=NULL;) {
       temp=p;
       p=p->next;
       free(temp);
    }
}
free(adjList);
}

struct node * createNode(int n)
{
struct node *i = (struct node *) malloc(sizeof(struct node));
i->id=n;
i->next=NULL;
return i;
}

struct node ** insertLink(int i, int j, struct node ** adjList)
{
struct node * temp = adjList[i];
adjList[i] = createNode(j);
adjList[i]->next = temp;
return adjList;
}

struct node ** createGraph1()
{
struct node ** graph1= createAdjList(5);
graph1 = insertLink(0,1,graph1);
graph1 = insertLink(0,4,graph1);
graph1 = insertLink(1,4,graph1);
graph1 = insertLink(1,2,graph1);
graph1 = insertLink(2,3,graph1);
graph1 = insertLink(3,2,graph1);
graph1 = insertLink(3,4,graph1);
graph1 = insertLink(4,2,graph1);
graph1 = insertLink(4,3,graph1);
graph1 = insertLink(4,0,graph1);
return graph1;
}

struct node ** createTranspose(struct node ** adjList, int n)
{
   struct node ** transpose = createAdjList(n);

   struct node * p;
   for (int i=0; i<n; i++) {
      for (p=adjList[i]; p!= NULL; p=p->next) {
         transpose = insertLink(p->id,i,transpose);
      }
   }

   return transpose;
}


void displayAdjList(struct node ** adjList, int n)
{
struct node * p;
for (int i=0; i<n; i++) {
   printf("%2d:",i);
   for (p=adjList[i]; p!= NULL; p=p->next) {
      printf("->%2d",p->id);
   }
   printf("\n");
}
printf("\n");
}

int main()
{
struct node ** graph1 = createGraph1();
printf("Adjacency list of graph 1\n");
displayAdjList(graph1, 5);

struct node ** transpose = createTranspose(graph1,5);
printf("Adjacency list of transpose\n");
displayAdjList(transpose, 5);

destroyAdjList(graph1,5);
destroyAdjList(transpose,5);
}
