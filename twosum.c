#include <stdlib.h>
#include <stdio.h>

#define FAIL -1

struct node {
	int key;
	int value;
	struct node * next;
};

struct hashTable {
	struct node ** table;
	int size;
};

struct hashTable * createHTable(int size)
{
struct hashTable * ht=(struct hashTable *)malloc(sizeof(struct hashTable));
ht->size=size;
ht->table=(struct node**)malloc(size*sizeof(struct node *));
for (int i=0; i<ht->size; i++) {
    ht->table[i]=NULL;
}
return ht;
}

void destroyHTable(struct hashTable * ht)
{
for (int i=0; i<ht->size; i++) {
    for (struct node *p=ht->table[i]; p!=NULL;) {
            struct node *tmp = p;
    	    p=p->next;
	    free(tmp);
    }
}
free(ht);
}

int hash(int key, int size)
{
return (key*67+83)%size;
}

void insertHTable(struct hashTable *ht, int key, int value)
{
int index=hash(key,ht->size);
struct node * p=(struct node*)malloc(sizeof(struct node));
p->key=key;
p->value=value;
p->next=ht->table[index];
ht->table[index]=p;
}

int findHTable(struct hashTable *ht, int key)
{
int index=hash(key,ht->size);
for (struct node* p=ht->table[index]; p!=NULL; p=p->next) {
    if (p->key==key) return p->value;
}
return FAIL;
}

void twoSum(int* nums, int numsSize, int target, int* result)
{
   struct hashTable * ht = createHTable(64);

   result[0] = FAIL;
   result[1] = FAIL;
   for(int i = 0; i < numsSize; i++) {
      int key = target - nums[i];
      if(findHTable(ht,key) != FAIL) {
         result[0] = findHTable(ht,key);
         result[1] = i;
         return;
      } else {
         insertHTable(ht, nums[i], i);
      }
   }
}


void main()
{
int result[2];

int nums1[4] = {2,7,11,15};
int target1=9;
twoSum(nums1, 4, target1, result);
printf("Example 1: target %4d, result: %3d %3d\n",target1,result[0],result[1]);

int nums2[4] = {2,7,2,15};
int target2=4;
twoSum(nums2, 4, target2, result);
printf("Example 2: target %4d, result: %3d %3d\n",target2,result[0],result[1]);

int nums3[4] = {3,2,4};
int target3=6;
twoSum(nums3, 3, target3, result);
printf("Example 3: target %4d, result: %3d %3d\n",target3,result[0],result[1]);

int nums4[10] = {33,2,45,71,29,38,25,9,71,14};
int target4=54;
twoSum(nums4, 10, target4, result);
printf("Example 4: target %4d, result: %3d %3d\n",target4,result[0],result[1]);

}
