#include <stdlib.h>
#include <stdio.h>

struct Node {
	int val;
	struct Node * next;
};

void displayList(struct Node *head);
void destroyList(struct Node *head);
int listSize(struct Node* head);
struct Node * initList(int n); 
struct Node * weave(struct Node * head);
struct Node * rotate_right(struct Node * head, int k);


void main(void) 
{
struct Node * head;
printf("Weave an even length list\n");
head = initList(10);
displayList(head);
head = weave(head);
displayList(head);
destroyList(head);

printf("Weave an odd length list\n");
head = initList(9);
displayList(head);
head = weave(head);
displayList(head);
destroyList(head);

printf("Rotate to the right by 7 nodes\n");
head = initList(10);
displayList(head);
head = rotate_right(head,7);
displayList(head);
destroyList(head);
}

struct Node * weave(struct Node * head)
{
   if(head == NULL || head->next == NULL) {
      return head;
   }

   struct Node* list1 = head;
   struct Node* list2 = head;
   struct Node* runner = head;
   int size = listSize(head);

   for(int i=0; i<(size+1)/2; i++) {
      list2 = list2->next;
   }

   while(list2 != NULL) {
      struct Node* temp1 = list1->next;
      struct Node* temp2 = list2->next;

      list1->next = list2;
      if(size%2 == 0 && temp2 == NULL) {
         break;
      }
      list2->next = temp1;
          
      list1 = temp1;
      list2 = temp2; 
   }
   if(size%2 == 1) {
      list1->next = NULL;
   }
   
   return head;
}

struct Node * rotate_right(struct Node * head, int k)
{
   struct Node* newHead = NULL;
   struct Node* newTail = NULL;

   for(int i=0; i<k; i++) {
      struct Node* oldHead = head;
      while(head->next != NULL) {
         newTail = head;
         newHead = head->next;
         head = head->next;
      }

      newHead->next = oldHead;
      newTail->next = NULL;
   }

   return newHead;
}

int listSize(struct Node* head) {
   int size = 0;
   while(head != NULL) {
      size++;
      head = head->next;
   }
   return size;
}

struct Node * createNode(int val)
{
struct Node * p = (struct Node *) malloc(sizeof(struct Node));
p->val = val;
p->next = NULL;
return p;
}

void destroyNode(struct Node *p)
{
free(p);
}


struct Node * initList(int n) 
{
if (n<=0) return NULL;
struct Node * head = createNode(0);
struct Node * p = head;
for (int i=1; i<n; i++) {
    p->next = createNode(i);
    p = p->next;
}
return head;
}

void destroyList(struct Node *head)
{
for (struct Node *p = head; p!=NULL;) {
    struct Node * temp = p;
    p = p->next;
    free(temp);
}
}

void displayList(struct Node *head)
{
for (struct Node *p = head; p!=NULL; p=p->next) {
	printf("->%d",p->val); 
}
printf("\n");
}



